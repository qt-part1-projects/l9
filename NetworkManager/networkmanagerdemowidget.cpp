#include "networkmanagerdemowidget.h"
#include "ui_networkmanagerdemowidget.h"

#include <QNetworkReply>

NetworkManagerDemoWidget::NetworkManagerDemoWidget( QWidget* parent ) :
    QWidget( parent ),
    ui( new Ui::NetworkManagerDemoWidget ) {
    ui->setupUi( this );

    connect( ui->bnGo, SIGNAL( clicked( bool ) ), SLOT( onGo() ) );
    //TODO: add your code here
}

NetworkManagerDemoWidget::~NetworkManagerDemoWidget() {
    delete ui;
}

void NetworkManagerDemoWidget::onGo() {
    ui->lbStatus->setText( "Working..." );
    ui->progressBar->setValue(0);

    //TODO: add your code here
}

void NetworkManagerDemoWidget::onFinished( QNetworkReply* reply ) {
  //TODO: add your code here
}

//TODO: add your code here
